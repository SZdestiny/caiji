﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModBusTCP_Slave_for_CSV
{
    public class GloalVariables
    {
        public string connectString = "";
        public string ProjectGuid = "";
        public string mqttSvr = "";
        public string serverID = "";
        public string serverName = "";
        public int mqttPort;
        public int mqttReconnectTime;
        public bool uSTenable;
        public int updatePeriod;
    }
}
