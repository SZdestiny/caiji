﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Runtime.InteropServices;
using System.Collections;

namespace ModBusTCP_Slave_for_CSV
{   
    class DataBlock
    {
        public int startAdd;//数据块开始地址
        public TAG_TYPE tagType;//功能码
        public ArrayList Tag;
    }
    class TAG
    {
        public int modAdd;//地址
        public TAG_TYPE tagType;//功能码
        public int length;//读取长度
        public string type;//数据类型
        public object value;//值
        public float rate;//倍率
        public string name;//参数标准名
    }
    enum TAG_TYPE
    {
        coilstatus,//功能码1 读取线圈状态
        inputStatus,//功能码2 读取输入状态
        holdingregister,//功能码3 读取保存寄存器 
        inputregister,//功能码4 读取输入寄存器
        other
    }
}
