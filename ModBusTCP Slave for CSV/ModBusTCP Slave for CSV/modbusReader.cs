﻿using System;
using System.Collections.Generic;
/*using*/
//System.Linq;
using System.Text;
using System.Collections;
//using Modbus.Device;
using System.Net.Sockets;
using System.Threading;
using Modbus.Device;
using log4net;
using System.Data.SqlClient;

namespace ModBusTCP_Slave_for_CSV
{
    class ModbusReader
    {
        public static readonly ILog _log = LogManager.GetLogger("");
        private int m_modbusPort = 502;
        private int m_nTimeOut = 10000;
        private int m_nSleepTime = 10;

        public ModbusReader(ArrayList ALLList, int modbusPort, int nTimeOut, int nSleepTime)
        {
            for (int i = 0; i < ALLList.Count; i++)
            {
                m_modbusPort = modbusPort;
                m_nTimeOut = nTimeOut;
                m_nSleepTime = nSleepTime;

                //每个机型都创建一个线程，采集数据 ReadModbus

                ArrayList a_tmp = (ArrayList)ALLList[i];
                Thread tReadModbus = new Thread(new ParameterizedThreadStart(Read));
                tReadModbus.IsBackground = true; ;
                tReadModbus.Start(a_tmp);
                _log.InfoFormat("线程{0}启动", i + 1);
            }
        }

        public void Read(object o)
        {
            //声明一个键值对 用来存储ip和连接
            Dictionary<string, TcpClient> d = new Dictionary<string, TcpClient>();
            //创建sqlserver连接
            InsertSqlServer isql = new InsertSqlServer();
            SqlConnection con = null;
            bool b = false;
            while (!b)//
            {
                b = isql.SqlConnect(ref con);//连接本地数据
                if (!b)
                    Thread.Sleep(5000);
            }
            while (true)
            {
                while (con.State == System.Data.ConnectionState.Closed)
                {
                    try
                    {
                        con.Open();
                    }
                    catch (System.Exception ex)
                    {
                        _log.ErrorFormat(ex.Message.ToString());
                    }

                }
                ReadModbus(o, ref d, isql, con);
                _log.InfoFormat("当前线程设备数量为：{0}", d.Count);
                Thread.Sleep(MainForm.nTime);
            }


        }

        public void ReadModbus(object o, ref Dictionary<string, TcpClient> dd, InsertSqlServer isql, SqlConnection con)
        {
            ArrayList a = (ArrayList)o;
            for (int i = 0; i < a.Count; i++)
            {
                ArrayList a_tmp = (ArrayList)a[i];//取出一个ip信息 
                ArrayList cmdList = (ArrayList)a_tmp[3];//协议内容
                int state = 2001;
                if (true)//如果ip没有进行过连接
                {

                    TcpClient client_tmp = new TcpClient();
                    try
                    {
                        //生成一个连接
                        client_tmp = new TcpClientWithTimeout((string)a_tmp[0], m_modbusPort, 500).Connect();
                        _log.Info("连接成功IP=" + a_tmp[0]);

                        if (client_tmp.Connected)//如果连接正常 读取数据
                        {
                            client_tmp.ReceiveTimeout = m_nTimeOut;//数据返回超时时间
                            ModbusIpMaster m_master = ModbusIpMaster.CreateIp(client_tmp);

                            m_master.Transport.Retries = 0;
                            for (byte m_slaveAdd = (byte)a_tmp[1]; m_slaveAdd <= (byte)a_tmp[2]; m_slaveAdd++)
                            {
                                if (client_tmp.Connected)
                                {
                                    for (int j = 0; j < cmdList.Count; j++)
                                    {
                                        DataBlock datablock = (DataBlock)cmdList[j];
                                        bool b = ReadSingle(m_master, m_slaveAdd, ref datablock);
                                        if (b == false)
                                        {
                                            state = 2003; //如果读取失败一次就不再读了
                                                          //插入数据库 此站号是通信异常的
                                            //isql.ExecuteInsert(con, a_tmp, m_slaveAdd, state);
                                            _log.ErrorFormat("数据返回异常IP={0},id={1},数据块={2}", (string)a_tmp[0], m_slaveAdd, j);
                                            break;
                                        }
                                        Thread.Sleep(m_nSleepTime);
                                    }
                                    //数据读取完毕要插入数据库
                                    state = 2001;
                                    string s = string.Empty;
                                    isql.ExecuteInsert(con, a_tmp, m_slaveAdd, state);
                                }
                                else
                                {
                                    state = 2003;
                                    _log.ErrorFormat("连接异常IP={0},id={1}", (string)a_tmp[0], m_slaveAdd);
                                    //isql.ExecuteInsert(con, a_tmp, m_slaveAdd, state);
                                }
                            }
                            m_master.Dispose();
                        }
                        else
                        {
                            //d.Remove((string)a_tmp[0]);//连接不正常就删除这个连接记录
                            client_tmp.Close();
                            _log.ErrorFormat("连接异常IP={0},断开连接", (string)a_tmp[0]);

                            for (int num = int.Parse(a_tmp[1].ToString()); num <= int.Parse(a_tmp[2].ToString()); num++)
                            {
                                //插入数据库 ip下所有站号都是通信异常的
                                state = 2003;
                                //isql.ExecuteInsert(con, a_tmp, num, state);
                            }
                        }
                        client_tmp.Close();

                    }
                    catch (System.Exception ex)
                    {
                        _log.WarnFormat("连接失败IP={0}\t{1}", (string)a_tmp[0], ex.Message.ToString());
                        state = 2003;
                        for (int num = (byte)a_tmp[1]; num <= (byte)a_tmp[2]; num++)
                        {
                            //插入数据库 ip下所有站号都是通信异常的
                            isql.ExecuteInsert(con, a_tmp, num, state);
                        }
                    }
                }
                else//进行过连接
                {
                    

                }
            }

        }

        public bool ReadSingle(ModbusIpMaster master, byte id, ref DataBlock datablock)
        {
            for (int i = 0; i < datablock.Tag.Count; i++)
            {
                ((TAG)datablock.Tag[i]).value = null;
            }
            try
            {
                switch (datablock.tagType)
                {
                    #region 读取保存寄存器(功能码1)
                    case TAG_TYPE.coilstatus:
                        {
                            bool[] uRes;
                            ushort len = 16;
                            //if (datablock.Tag.Count != 1)
                            //{
                            //    len = (ushort)(((TAG)datablock.Tag[datablock.Tag.Count - 1]).modAdd - ((TAG)datablock.Tag[0]).modAdd);
                            //}
                            uRes = master.ReadCoils(id, (ushort)datablock.startAdd, (ushort)(len));
                            for (int i = 0; i < datablock.Tag.Count; i++)
                            {
                                byte[] val = new byte[2];
                                for (int k = 0; k < 2; k++)
                                {
                                    for (int kk = 0; kk < 8; kk++)
                                    {
                                        int v = uRes[kk + 8 * k] == true ? 1 : 0;
                                        int tmp = v << kk | val[k];
                                        val[k] = byte.Parse(tmp.ToString());
                                    }
                                }
                                ((TAG)datablock.Tag[i]).value = BitConverter.ToUInt16(Swap16(ref val, ((TAG)datablock.Tag[i]).type), 0);
                            }
                            break;
                        }
                    #endregion
                    #region 读取保存寄存器(功能码2)
                    case TAG_TYPE.inputStatus:
                        {
                            bool[] uRes;
                            ushort len = 16;
                            //if (datablock.Tag.Count != 1)
                            //{
                            //    len = (ushort)(((TAG)datablock.Tag[datablock.Tag.Count - 1]).modAdd - ((TAG)datablock.Tag[0]).modAdd);
                            //}
                            uRes = master.ReadInputs(id, (ushort)datablock.startAdd, (ushort)(len));
                            for (int i = 0; i < datablock.Tag.Count; i++)
                            {
                                byte[] val = new byte[2];
                                for (int k = 0; k < 2; k++)
                                {
                                    for (int kk = 0; kk < 8; kk++)
                                    {
                                        int v = uRes[kk + 8 * k] == true ? 1 : 0;
                                        int tmp = v << kk | val[k];
                                        val[k] = byte.Parse(tmp.ToString());
                                    }
                                }
                                ((TAG)datablock.Tag[i]).value = BitConverter.ToUInt16(Swap16(ref val, ((TAG)datablock.Tag[i]).type), 0);
                            }
                            break;
                        }
                    #endregion
                    #region 读取保存寄存器(功能码3)
                    case TAG_TYPE.holdingregister:
                        {
                            ushort[] uRes;
                            ushort len = 0;
                            if (datablock.Tag.Count != 1)
                            {
                                len = (ushort)(((TAG)datablock.Tag[datablock.Tag.Count - 1]).modAdd - ((TAG)datablock.Tag[0]).modAdd);
                            }
                            int len_last = ((TAG)datablock.Tag[datablock.Tag.Count - 1]).length / 2;
                            uRes = master.ReadHoldingRegisters(id, (ushort)datablock.startAdd, (ushort)(len + len_last));
                            int Offset = 0;

                            for (int i = 0; i < datablock.Tag.Count; i++)
                            {
                                TAG current_t = (TAG)datablock.Tag[i];


                                ((TAG)datablock.Tag[i]).value = m_BitConverter(uRes, current_t, ref Offset);
                                if (i + 1 < datablock.Tag.Count)
                                {
                                    TAG next_t = (TAG)datablock.Tag[i + 1];
                                    Offset += ((next_t.modAdd - current_t.modAdd) * 2);
                                }
                            }

                            break;
                        }
                    #endregion
                    #region 读取保存寄存器(功能码4)
                    case TAG_TYPE.inputregister:
                        {
                            ushort[] uRes;
                            ushort len = 0;
                            if (datablock.Tag.Count != 1)
                            {
                                len = (ushort)(((TAG)datablock.Tag[datablock.Tag.Count - 1]).modAdd - ((TAG)datablock.Tag[0]).modAdd);
                            }
                            uRes = master.ReadInputRegisters(id, (ushort)datablock.startAdd, (ushort)(len + 2));
                            int Offset = 0;
                            for (int i = 0; i < datablock.Tag.Count; i++)
                            {
                                TAG current_t = (TAG)datablock.Tag[i];
                                ((TAG)datablock.Tag[i]).value = m_BitConverter(uRes, current_t, ref Offset);
                                if (i + 1 < datablock.Tag.Count)
                                {
                                    TAG next_t = (TAG)datablock.Tag[i + 1];
                                    Offset += ((next_t.modAdd - current_t.modAdd) * 2);
                                }
                            }

                            break;
                        }
                    #endregion
                }
                return true;
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return false;
            }

        }

        public object m_BitConverter(ushort[] uRes, TAG tag, ref int Offset)
        {
            if (tag.type.Contains("16"))
            {
                byte[] b = new byte[2];
                Buffer.BlockCopy(uRes, Offset, b, 0, 2);
                if (tag.type.Contains("uint16"))
                    return BitConverter.ToUInt16(Swap16(ref b, tag.type), 0);
                if (tag.type.Contains("bcd16"))
                {
                    b = Swap16(ref b, tag.type);
                    int bcd = 0;
                    int j = 1;
                    for (int i = 0; i < b.Length; i++)
                    {
                        int tmp = b[i] & 0x0f;
                        bcd += tmp * j;
                        j *= 10;
                        tmp = (b[i] >> 4) & 0x0f;
                        bcd += tmp * j;
                        j *= 10;
                    }
                    return bcd;
                }
                return BitConverter.ToInt16(Swap16(ref b, tag.type), 0);
            }
            else if (tag.type.Contains("32"))
            {
                byte[] b = new byte[4];
                Buffer.BlockCopy(uRes, Offset, b, 0, 4);
                if (tag.type.Contains("uint32"))
                    return BitConverter.ToUInt32(Swap32(ref b, tag.type), 0);
                if (tag.type.Contains("bcd32"))
                {
                    b = Swap32(ref b, tag.type);
                    int bcd = 0;
                    int j = 1;
                    for (int i = 0; i < b.Length; i++)
                    {
                        int tmp = b[i] & 0x0f;
                        bcd += tmp * j;
                        j *= 10;
                        tmp = (b[i] >> 4) & 0x0f;
                        bcd += tmp * j;
                        j *= 10;
                    }
                    return bcd;
                }
                return BitConverter.ToInt32(Swap32(ref b, tag.type), 0);
            }
            else if (tag.type.Contains("64"))
            {
                byte[] b = new byte[8];
                Buffer.BlockCopy(uRes, Offset, b, 0, 8);
                if (tag.type.Contains("uint64"))
                    return BitConverter.ToUInt64(Swap64(ref b, tag.type), 0);
            }
            else if (tag.type.Contains("float"))
            {
                byte[] b = new byte[4];
                Buffer.BlockCopy(uRes, Offset, b, 0, 4);
                return (float)Math.Round(BitConverter.ToSingle(Swap32(ref b, tag.type), 0), 4);
            }
            else if (tag.type.Contains("ascii"))
            {
                byte[] bs = new byte[125 * 2];
                for (int ii = 0; ii < uRes.Length; ii++)
                {
                    byte temp = (byte)((uRes[ii] & 0xff00) >> 8);
                    if (temp == 0x40)
                    {
                        break;
                    }
                    else
                    {
                        bs[2 * ii] = temp;
                    }
                    temp=(byte)(uRes[ii] & 0x00ff);
                    if (temp == 0x40)
                    {
                        break;
                    }
                    else
                    {
                        bs[2 * ii + 1] = temp;
                    }
                }
                string s = System.Text.Encoding.ASCII.GetString(bs);
                return s.Replace("\r\n", "").Replace("\0", "");
            }
            return 0x1111;

        }

        //字节序交换
        private byte[] Swap64(ref byte[] objects, string type)
        {
            byte[] temp = new byte[8];
            temp[0] = objects[0];
            temp[1] = objects[1];
            temp[2] = objects[2];
            temp[3] = objects[3];
            temp[4] = objects[4];
            temp[5] = objects[5];
            temp[6] = objects[6];
            temp[7] = objects[7];
            switch (type)
            {
                case "int64_1":
                case "uint64_1":
                    {
                        objects[0] = temp[6];
                        objects[1] = temp[7];
                        objects[2] = temp[4];
                        objects[3] = temp[5];
                        objects[4] = temp[2];
                        objects[5] = temp[3];
                        objects[6] = temp[0];
                        objects[7] = temp[1];
                    }
                    break;

                default:
                    break;
            }
            return objects;
        }

        //字节序交换
        private byte[] Swap32(ref byte[] objects, string type)
        {
            byte[] temp = new byte[4];
            temp[0] = objects[0];
            temp[1] = objects[1];
            temp[2] = objects[2];
            temp[3] = objects[3];
            switch (type)
            {
                case "int32_1":
                case "uint32_1":
                case "bcd32_1":
                case "float_1":
                    {
                        objects[0] = temp[2];
                        objects[1] = temp[3];
                        objects[2] = temp[0];
                        objects[3] = temp[1];
                    }
                    break;

                case "int32_2":
                case "uint32_2":
                case "bcd32_2":
                case "float_2":
                    {
                        objects[0] = temp[3];
                        objects[1] = temp[2];
                        objects[2] = temp[1];
                        objects[3] = temp[0];
                    }
                    break;
                case "int32_3":
                case "uint32_3":
                case "bcd32_3":
                case "float_3":
                    {
                        objects[0] = temp[1];
                        objects[1] = temp[0];
                        objects[2] = temp[3];
                        objects[3] = temp[2];
                    }
                    break;

                default:
                    break;
            }
            return objects;
        }

        //字节序交换
        private byte[] Swap16(ref byte[] objects, string type)
        {
            byte[] temp = new byte[2];
            temp[0] = objects[0];
            temp[1] = objects[1];
            switch (type)
            {
                case "int16_1":
                case "uint16_1":
                case "bcd16_1":
                    {
                        objects[1] = temp[0];
                        objects[0] = temp[1];
                    }
                    break;

                default:
                    break;
            }
            return objects;
        }
    }

}