﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using uPLibrary.Networking.M2Mqtt;
using System.Threading;
using uPLibrary.Networking.M2Mqtt.Messages;
using log4net;

namespace ModBusTCP_Slave_for_CSV
{
    public class whdMqtt
    {
        public static readonly ILog _log = LogManager.GetLogger("");
        public MqttClient mqttClient = null;
        static bool LastConnected = false;
        static Thread tConnect = null;
        GloalVariables gVs = null;
        public whdMqtt(GloalVariables gV)
        {
            gVs = gV;
            iniMqttClient();
            //断线检测重连
            tConnect = new Thread(new ParameterizedThreadStart(reConnect));
            tConnect.Start(gVs.mqttReconnectTime);
        }

        void iniMqttClient()
        {
            try
            {
                string enpoint = gVs.mqttSvr;//后台http://test.jwrx.net:61680
                int port = gVs.mqttPort;

                mqttClient = new MqttClient(enpoint, port, false, MqttSslProtocols.None, null, null);

                byte code = mqttClient.Connect(gVs.ProjectGuid + Guid.NewGuid(), "mqtt-test", "123456", false, MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, true, "DisConn", "RemoteDevOps" + "计划外关闭", true, 30);
                mqttClient.ConnectionClosed += Client_ConnectionClosed;//连接断开了
                mqttClient.MqttMsgPublished += MqttClient_MqttMsgPublished;//发送完毕
                if (mqttClient.IsConnected && code == 0)
                {
                    LastConnected = true;
                    //连接成功了
                    _log.Info("mqtt连接成功");
                }
                else
                {
                    LastConnected = false;
                }
            }
            catch (Exception ex)
            {
                _log.ErrorFormat( "mqtt连接异常\t"+ex.Message);
                Console.WriteLine(ex.Message);
            }
        }

        void reConnect(object tick)
        {
            int p = (int)tick;
            while (true)
            {
                if (LastConnected && !mqttClient.IsConnected)
                {
                    _log.Info("断开连接了");
                }
                if (mqttClient == null || !mqttClient.IsConnected)
                {
                    iniMqttClient();
                }
                Thread.Sleep(p);
            }
        }

        static void MqttClient_MqttMsgPublished(object sender, MqttMsgPublishedEventArgs e)
        {
            //发送完毕消息            
        }
        static void Client_ConnectionClosed(object sender, EventArgs e)
        {
            //服务器连接断开了
            Console.WriteLine("已经断开连接");
            _log.Error("已经断开连接" + "\tClient_ConnectionClosed");
        }
    }
}
