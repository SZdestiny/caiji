﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Collections;
using System.Windows.Forms;

namespace ModBusTCP_Slave_for_CSV
{
    class CsvReader
    {
        private ArrayList m_cmdList;
        public CsvReader(ref ArrayList cmdList)
        {
            m_cmdList = cmdList;
        }

        public bool ReadCsv(string name)
        {
            try
            {
                string strPath = "";//System.Environment.CurrentDirectory;
                strPath += "D:\\Work\\gathering\\config文件\\config";
                strPath += (name + ".csv");
                CsvStreamReader csvReader = new CsvStreamReader(strPath);
                for (int i = 2; i <= csvReader.RowCount; i++)
                {
                    TAG tag = new TAG();
                    tag.modAdd = Convert.ToInt32(csvReader[i, 2]);//参数地址

                    switch (csvReader[i, 3])//功能码
                    {
                        case "1":
                            tag.tagType = TAG_TYPE.coilstatus;
                            break;
                        case "2":
                            tag.tagType = TAG_TYPE.inputStatus;
                            break;
                        case "3":
                            tag.tagType = TAG_TYPE.holdingregister;
                            break;
                        case "4":
                            tag.tagType = TAG_TYPE.inputregister;
                            break;
                        default:
                            tag.tagType = TAG_TYPE.other;
                            break;
                    }

                    tag.type = Convert.ToString(csvReader[i, 4]);//数据类型

                    if (tag.type.Contains("16"))//数据长度(字节)
                        tag.length = 2;
                    else if (tag.type.Contains("64"))
                        tag.length = 8;
                    else if (tag.type.Contains("ascii"))
                        tag.length = int.Parse(tag.type.Replace("ascii", ""));
                    else
                        tag.length = 4;

                    if (!float.TryParse(csvReader[i, 5], out tag.rate))
                    {
                        tag.rate = 1;
                    }

                    tag.name = csvReader[i, 6];//参数名

                    if (tag.tagType != TAG_TYPE.other && tag.type != null)
                    {
                        if (csvReader[i, 1] != "")
                        {

                            DataBlock datablock = new DataBlock();
                            datablock.startAdd = tag.modAdd;
                            datablock.tagType = tag.tagType;
                            ArrayList tmp;
                            if (datablock.Tag == null)
                                tmp = new ArrayList();
                            else
                                tmp = datablock.Tag;
                            tmp.Add(tag);
                            datablock.Tag = tmp;
                            m_cmdList.Add(datablock);
                        }
                        else
                        {
                            DataBlock tmp = (DataBlock)m_cmdList[m_cmdList.Count - 1];
                            tmp.Tag.Add(tag);
                            m_cmdList[m_cmdList.Count - 1] = tmp;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                return false;
            }
            return true;
        }
    }
}
