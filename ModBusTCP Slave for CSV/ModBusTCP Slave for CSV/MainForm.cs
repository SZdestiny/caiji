﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using log4net;
using System.Threading;
using System.Collections;

namespace ModBusTCP_Slave_for_CSV
{
    public partial class MainForm : Form
    {
        public static readonly ILog _log = LogManager.GetLogger("");

        private AutoResetEvent sendEvent;
        private int modbusPort = 502;               //modbus端口号
        public static int nTime;                    //机台读取间隔时间
        public static int nSleepTime;               //指令间时间间隔
        public static int nTimeOut;                 //超时时间


        private ArrayList ALL = new ArrayList();    //MasterType的集合 存储机型、ip、站号、协议、参数名等所有信息

        public MainForm()
        {
            log4net.Config.XmlConfigurator.Configure();
            InitializeComponent();
            sendEvent = new AutoResetEvent(false);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            try
            {
                _log.Info("程序已经启动");
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
            ini_analysis();

            ModbusReader mr = new ModbusReader(ALL, modbusPort, nTimeOut, nSleepTime);
        }

        private bool ini_analysis()
        {
            try
            {
                Ini ini = new Ini("config4.ini");//读取配置文件
                string MasterTypeList = ini.ReadValue("配置", "机型列表");
                nTimeOut = int.Parse(ini.ReadValue("配置", "超时时间"));
                nTime = int.Parse(ini.ReadValue("配置", "主机间时间间隔"));
                nSleepTime = int.Parse(ini.ReadValue("配置", "指令间时间间隔"));
                string[] s_tmp = MasterTypeList.Split(',');//机型列表的数组
                for (int j = 0; j < s_tmp.Length; j++)
                {
                    ArrayList MasterType = new ArrayList();                     //MasterInfo的集合
                    string modbusinfo = ini.ReadValue(s_tmp[j], "设备信息");    //读取不同机型列表下的 设备信息(ip、站号)
                    string SPName = ini.ReadValue(s_tmp[j], "存储过程名");      //存储过程名
                    string SPMode = ini.ReadValue(s_tmp[j], "模式");      //模式
                    string[] modbusinfo_tmp = modbusinfo.Split(',');
                    ArrayList cmdList = new ArrayList();                        //存储协议内容
                    for (int i = 0; i < modbusinfo_tmp.Length; i++)
                    {
                        ArrayList MasterInfo = new ArrayList();                 //ip、站号开始、站号结束、协议                    
                        int i_start = modbusinfo_tmp[i].IndexOf('(');
                        int i_end = modbusinfo_tmp[i].IndexOf(')');
                        modbusinfo = modbusinfo_tmp[i].Substring(0, i_start);
                        string s_addr_tmp = modbusinfo_tmp[i].Substring(i_start + 1, i_end - i_start - 1);
                        i_start = s_addr_tmp.IndexOf('-');
                        byte slaveAdd_start = byte.Parse(s_addr_tmp.Substring(0, i_start));
                        byte slaveAdd_end = byte.Parse(s_addr_tmp.Substring(i_start + 1, s_addr_tmp.Length - i_start - 1));

                        MasterInfo.Add(modbusinfo);
                        MasterInfo.Add(slaveAdd_start);
                        MasterInfo.Add(slaveAdd_end);
                        if (i == 0)
                        {
                            CsvReader reader = new CsvReader(ref cmdList);
                            if (reader.ReadCsv(s_tmp[j]) == false)
                            {
                                _log.Error("ini、csv文件解析异常");
                            }
                            else
                            {
                                _log.Info("ini、csv文件解析完毕");
                            }
                        }
                        MasterInfo.Add(cmdList);
                        MasterInfo.Add(SPName);
                        MasterInfo.Add(SPMode);
                        MasterType.Add(MasterInfo);
                    }
                    ALL.Add(MasterType);//存储所有机型信息
                }
                return true;
            }
            catch (Exception ex)
            {
                _log.ErrorFormat("采集程序初始化失败,请检查ini、csv文件。错误信息：{0}", ex.Message);
                return false;
            }

        }
    }
}
