﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Collections;
using log4net;
using System.Threading;
using System.Xml;
using Newtonsoft.Json;
using System.IO;
using System.IO.Compression;

namespace ModBusTCP_Slave_for_CSV
{
    class InsertSqlServer
    {
        // 标记
        public static readonly ILog _log = LogManager.GetLogger("");
        static GloalVariables gVs = new GloalVariables() { mqttPort = 1883, mqttSvr = "101.200.182.225", ProjectGuid = "1A0B5B01-D915-4EB5-B691-A1A77776E1F3" };
        whdMqtt myMqtt = new whdMqtt(gVs);
        public bool SqlConnect(ref SqlConnection con)
        {
            string s = "server=localhost;database=" + new Ini("config.ini").ReadValue("配置", "数据库") + ";uid=sa;pwd=824889784";
            try
            {
                con = new SqlConnection(s);
                con.Open();
                return true;
            }
            catch (System.Exception ex)
            {
                _log.Error(ex.Message);
                return false;
            }
        }

        public bool ExecuteInsert(SqlConnection con, ArrayList a_tmp, int num, int state)
        {
            try
            {
                if ((DateTime.Now.Hour % 12 == 0) && (DateTime.Now.Minute == 30))
                {
                    Environment.Exit(0);//结束当前进程
                }

                SqlCommand com = new SqlCommand((string)a_tmp[4], con);
                com.CommandType = CommandType.StoredProcedure;

                if ((string)a_tmp[5] == "xml")
                {
                    string xml = string.Format("<root>\r\n<IP>{0}</IP>\r\n<Address>{1}</Address>\r\n<device_state>{2}</device_state>\r\n<ProjectGuid>{3}</ProjectGuid>\r\n", (string)a_tmp[0], num, state, gVs.ProjectGuid);

                    if (state == 2001)
                    {
                        ArrayList m_DataBlock = (ArrayList)a_tmp[3];//取出协议内容，包括参数名和值
                        for (int i = 0; i < m_DataBlock.Count; i++)
                        {
                            for (int j = 0; j < ((ArrayList)((DataBlock)m_DataBlock[i]).Tag).Count; j++)
                            {
                                try
                                {
                                    TAG tag = (TAG)((ArrayList)((DataBlock)m_DataBlock[i]).Tag)[j];
                                    if (tag.value == null)
                                        return false;
                                    if (!tag.type.Contains("ascii"))
                                    {
                                        decimal d = decimal.Parse(tag.value.ToString());
                                        xml += string.Format("<{0}>{1}</{2}>\r\n", tag.name, d * (decimal)tag.rate, tag.name);
                                    }
                                    else
                                    {
                                        xml += string.Format("<{0}>{1}</{2}>\r\n", tag.name, tag.value.ToString(), tag.name);
                                    }

                                }
                                catch (Exception ex)
                                {
                                    string s = ex.Message;
                                }

                            }
                        }
                    }
                    xml += "</root>";
                    if (state != 2003)
                    {
                        myMqtt.mqttClient.Publish("MachineDatas", CompressBytes(Encoding.UTF8.GetBytes(xml)));
                        com.Parameters.AddWithValue("@root", xml);
                    }
                }
                else
                {
                    ArrayList m_DataBlock = (ArrayList)a_tmp[3];//取出协议内容，包括参数名和值

                    com.Parameters.AddWithValue("@IP", (string)a_tmp[0]);
                    com.Parameters.AddWithValue("@address", num);
                    bool is_device_state = false;
                    for (int i = 0; i < m_DataBlock.Count; i++)
                    {
                        for (int j = 0; j < ((ArrayList)((DataBlock)m_DataBlock[i]).Tag).Count; j++)
                        {
                            TAG tag = (TAG)((ArrayList)((DataBlock)m_DataBlock[i]).Tag)[j];
                            if (tag.value == null)
                                state = 2003;
                            if (tag.name == "device_state")
                            {
                                com.Parameters.AddWithValue("@device_state", state);
                                is_device_state = true;
                            }
                            else
                            {
                                object o = state == 2001 ? float.Parse(tag.value.ToString()) * tag.rate : 0;
                                com.Parameters.AddWithValue("@" + tag.name, o);
                            }
                        }
                    }
                    if (!is_device_state)
                    {
                        com.Parameters.AddWithValue("@device_state", state);
                    }
                    if (true && state != 2003)
                    {
                        string xml = string.Format("<root>\r\n<IP>{0}</IP>\r\n<Address>{1}</Address>\r\n<device_state>{2}</device_state>\r\n<ProjectGuid>{3}</ProjectGuid>\r\n", (string)a_tmp[0], num, state, gVs.ProjectGuid);


                        m_DataBlock = (ArrayList)a_tmp[3];//取出协议内容，包括参数名和值
                        for (int i = 0; i < m_DataBlock.Count; i++)
                        {
                            for (int j = 0; j < ((ArrayList)((DataBlock)m_DataBlock[i]).Tag).Count; j++)
                            {
                                try
                                {
                                    TAG tag = (TAG)((ArrayList)((DataBlock)m_DataBlock[i]).Tag)[j];
                                    if (tag.value == null)
                                        return false;
                                    if (!tag.type.Contains("ascii"))
                                    {
                                        decimal d = decimal.Parse(tag.value.ToString());
                                        xml += string.Format("<{0}>{1}</{2}>\r\n", tag.name, d * (decimal)tag.rate, tag.name);
                                    }
                                    else
                                    {
                                        xml += string.Format("<{0}>{1}</{2}>\r\n", tag.name, tag.value.ToString(), tag.name);
                                    }

                                }
                                catch (Exception ex)
                                {
                                    string s = ex.Message;
                                }

                            }
                        }
                        xml += "</root>";

                        myMqtt.mqttClient.Publish("MachineDatas", CompressBytes(Encoding.UTF8.GetBytes(xml)));


                    }

                }
                try
                {
                    SqlDataReader reader = com.ExecuteReader();//执行SQL语句
                    reader.Close();//关闭执行
                    return true;
                }
                catch (Exception ex)
                {
                    _log.ErrorFormat("数据插入异常IP={0},{1}", a_tmp[0], ex.Message);
                    if (con.State == System.Data.ConnectionState.Closed)
                    {
                        try
                        {
                            con.Open();
                            _log.ErrorFormat("数据库重连成功IP={0}", a_tmp[0]);
                        }
                        catch (Exception e)
                        {
                            _log.ErrorFormat("数据库重连异常IP={0},{1}", a_tmp[0], e.Message);
                        }

                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                _log.ErrorFormat("数据插入异常IP={0},{1}", a_tmp[0], ex.Message);
                return false;
            }
        }
        public static byte[] CompressBytes(byte[] bytes)
        {
            using (MemoryStream compressStream = new MemoryStream())
            {
                using (var zipStream = new GZipStream(compressStream, CompressionMode.Compress))
                    zipStream.Write(bytes, 0, bytes.Length);
                return compressStream.ToArray();
            }
        }
    }
}